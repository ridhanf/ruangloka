# Official Website of RuangLoka

This is the official website of
[*RuangLoka*](https://ruangloka.herokuapp.com/)
by [Ridhan Fadhilah](http://www.linkedin.com/in/ridhanf).

## License

All source code in the [RuangLoka](https://ruangloka.herokuapp.com/)
is available jointly under the MIT License and the Beerware License. See
[LICENSE](LICENSE) for details.

## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```

For more information, see the
[*RuangLoka* Website](https://ruangloka.herokuapp.com/).